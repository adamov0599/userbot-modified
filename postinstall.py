# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#

import os

from dotenv import load_dotenv
from telethon import TelegramClient

load_dotenv("config.env")

API_KEY = os.environ.get("API_KEY", "")
API_HASH = os.environ.get("API_HASH", "")

bot = TelegramClient('userbot', API_KEY, API_HASH)
bot.start()
os.system("sh init/start.sh")